let canvas = document.getElementById("myCanvas");
let ctx = canvas.getContext("2d");
document.getElementById("start").addEventListener("click", startGame);

let x = canvas.width / 2;
let y = canvas.height - 20;
let dx = 2;
let dy = -2;
let ballRadius = 12;
let paddleHeight = 10;
let paddleWidth = 100;
let paddleX = (canvas.width - paddleWidth) / 2;
let rightPressed = false;
let leftPressed = false;
let brickRowCount = 3;
let brickColumnCount = 12;
let brickWidth = 70;
let brickHeight = 20;
let brickPadding = 10;
let brickOffsetTop = 40;
let brickOffsetLeft = 25;
let score = 0;
let lives = 3;
let bricks = [];
let level = 1;
let highScore = localStorage.getItem("HighScore");
let colors = ["red", "green", "blue", "yellow", "blueviolet", "pink"];
let totalScore = 0;

function initializeBricks() {
    for (let c = 0; c < brickColumnCount; c++) {
        bricks[c] = [];
        for (let r = 0; r < brickRowCount; r++) {
            let randomNumber = Math.round((Math.random() * 5));
            bricks[c][r] = {
                x: 0,
                y: 0,
                status: 1,
                color: colors[randomNumber],
                score: randomNumber + 1
            };
            totalScore += bricks[c][r].score;
        }
    }
}
initializeBricks();

function startGame() {
    document.getElementById("rules").style.display = "none";
    setTimeout(() => draw(), 2000);
}

document.addEventListener("keydown", keyDownHandler);
document.addEventListener("keyup", keyUpHandler);
document.addEventListener("mousemove", mouseMoveHandler);

function mouseMoveHandler(e) {
    let relativeX = e.clientX - canvas.offsetLeft;
    if (relativeX > 0 && relativeX < canvas.width) {
        paddleX = relativeX - paddleWidth / 2;
    }
}

function keyDownHandler(e) {
    if (e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = true;
    } else if (e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = true;
    }
}

function collisionDetection() {
    for (let c = 0; c < brickColumnCount; c++) {
        for (let r = 0; r < brickRowCount; r++) {
            let b = bricks[c][r];
            if (b.status == 1) {
                if (x > b.x && x < b.x + brickWidth && y > b.y && y < b.y + brickHeight) {
                    dy = -dy;
                    b.status = 0;
                    score += bricks[c][r].score;
                    if (score > highScore) {
                        highScore = score;
                        localStorage.setItem("HighScore", highScore);
                    }
                    if (score === totalScore) {
                        level++;
                        for (let c = 0; c < brickColumnCount; c++) {
                            for (let r = 0; r < brickRowCount; r++) {
                                bricks[c][r].status = 1;
                            }
                        }
                        if (level === 2) {
                            brickRowCount = 4;
                            initializeBricks();
                            lives = 3;
                            paddleX = (canvas.width - paddleWidth) / 2;
                            ballRadius = 10;
                            dx = 3;
                            dy = -3;
                            x = canvas.width / 2;
                            y = canvas.height - 20;
                        }
                        if (level === 3) {
                            brickRowCount = 5;
                            initializeBricks();
                            lives = 3;
                            paddleX = (canvas.width - paddleWidth) / 2;
                            ballRadius = 9;
                            dx = 4;
                            dy = -4;
                            x = canvas.width / 2;
                            y = canvas.height - 20;
                        } else if (level === 4) {
                            alert("YOU WIN, CONGRATULATIONS!");
                            document.location.reload();
                        }
                    }
                }
            }
        }
    }
}

function keyUpHandler(e) {
    if (e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = false;
    } else if (e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = false;
    }
}

function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI * 2);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}

function drawPaddle() {
    ctx.beginPath();
    ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBricks();
    drawBall();
    drawPaddle();
    drawScore();
    drawLives();
    drawHighScore();
    drawLevel();
    collisionDetection();

    if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
        dx = -dx;
    }
    if (y + dy < ballRadius) {
        dy = -dy;
    } else if (y + dy > canvas.height - ballRadius) {
        if (x > paddleX && x < paddleX + paddleWidth) {
            dy = -dy;
        } else {
            lives--;
            if (!lives) {
                alert("GAME OVER");
                document.location.reload();
            } else {
                x = canvas.width / 2;
                y = canvas.height - 30;
                //                dx = 2;
                //                dy = -2;
                if (dx < 0) {
                    dx = -dx;
                }
                if (dy > 0) {
                    dy = -dy;
                }
                paddleX = (canvas.width - paddleWidth) / 2;
            }
        }
    }

    if (rightPressed) {
        paddleX += 5;
        if (paddleX + paddleWidth > canvas.width) {
            paddleX = canvas.width - paddleWidth;
        }
    } else if (leftPressed) {
        paddleX -= 5;
        if (paddleX < 0) {
            paddleX = 0;
        }
    }

    x += dx;
    y += dy;
    requestAnimationFrame(draw);
}

function drawBricks() {
    for (let c = 0; c < brickColumnCount; c++) {
        for (let r = 0; r < brickRowCount; r++) {
            if (bricks[c][r].status == 1) {
                let brickX = (c * (brickWidth + brickPadding)) + brickOffsetLeft;
                let brickY = (r * (brickHeight + brickPadding)) + brickOffsetTop;
                bricks[c][r].x = brickX;
                bricks[c][r].y = brickY;
                ctx.beginPath();
                ctx.rect(brickX, brickY, brickWidth, brickHeight);
                ctx.fillStyle = bricks[c][r].color;
                ctx.fill();
                ctx.closePath();
            }
        }
    }
}

function drawScore() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#000";
    ctx.fillText("Score: " + score, 8, 20);
}

function drawHighScore() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#000";
    ctx.fillText("HighScore: " + highScore, 300, 20);
}

function drawLevel() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#000";
    ctx.fillText("Level: " + level, 600, 20);
}

function drawLives() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#000";
    ctx.fillText("Lives: " + lives, canvas.width - 65, 20);
}
